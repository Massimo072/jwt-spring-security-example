import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Movie} from '../movie';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private baseUrl = 'http://localhost:8080/api/movies';

  constructor(private http: HttpClient) {
  }

  createMovie(movie: Movie): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/add`, movie);
  }

  getMoviesList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  updateMovie(id: number, value: any): Observable<any> {
    return this.http.put(`${this.baseUrl}` + '/update/' + id, value);
  }

 /* updateMovie(id: number, value: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }*/

  getMoviesById(id: any): Observable<any> {
    return this.http.get(`${this.baseUrl}` + '/getMovie/' + id);
  }

  deleteMovie(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}` + '/delete/' + id, { responseType: 'text' });
  }
/*

  deleteMovie(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }
*/

}
