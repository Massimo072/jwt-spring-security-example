export class Movie {
  id: number;
  name: string;
  genre: string;
  releaseDate: string;
  director: string;
  description: string;
}
