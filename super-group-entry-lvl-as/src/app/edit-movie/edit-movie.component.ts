import {Component, OnInit} from '@angular/core';
import {MovieService} from '../service/movie.service';
import {Movie} from '../movie';
import {Observable, Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-edit-movie',
  templateUrl: './edit-movie.component.html',
  styleUrls: ['./edit-movie.component.css']
})
export class EditMovieComponent implements OnInit {

  movie: Movie = new Movie();
  submitted = false;
  id: any;
  sub: any;

  constructor(private movieService: MovieService, private  route: ActivatedRoute) {

  }

  ngOnInit() {

   this.route.paramMap.subscribe(params => {
      console.log(params);
      this.id = params.get('id');
      this.movieService.getMoviesById(this.id).subscribe(data => {
        this.movie = data;
        console.log('sssssssssssssssssssssssssssssssssss');
        console.log(data);
      });
    });
  }
  newMovie(): void {
    this.submitted = false;
    this.movie = new Movie();
  }

  save() {
    console.log("hello");
    this.movieService.updateMovie(this.movie.id,
      {
        name: this.movie.name,
        genre: this.movie.genre,
        releaseDate: this.movie.releaseDate,
        director: this.movie.director,
        description: this.movie.description
      })
      .subscribe(
        data => {
          console.log(data);
          this.movie = data as Movie;
        },
        error => console.log(error));
  }


  onSubmit() {
    this.submitted = true;
    this.save();
  }
}
