import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListMoviesComponent} from './list-movies/list-movies.component';
import {EditMovieComponent} from './edit-movie/edit-movie.component';
import {LoginComponent} from './login/login.component';
import {CreateMovieComponent} from './create-movie/create-movie.component';
import {LogoutComponent} from "./logout/logout.component";
import {AuthGaurdService} from "./service/auth-guard.service";


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'movies', component: ListMoviesComponent, canActivate:[AuthGaurdService]},
  { path: 'add', component: CreateMovieComponent, canActivate:[AuthGaurdService]  },
  { path: 'edit/:id', component: EditMovieComponent, canActivate:[AuthGaurdService]  },
  { path: 'logout', component: LogoutComponent, canActivate:[AuthGaurdService]  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
