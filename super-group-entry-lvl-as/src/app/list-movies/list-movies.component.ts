import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Movie} from '../movie';
import {MovieService} from '../service/movie.service';

@Component({
  selector: 'app-list-movies',
  templateUrl: './list-movies.component.html',
  styleUrls: ['./list-movies.component.css']
})
export class ListMoviesComponent implements OnInit {

  movies: Movie[];


  constructor(private movieService: MovieService) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
     this.movieService.getMoviesList().subscribe(data => {
      this.movies = data;
    });
  }

}
