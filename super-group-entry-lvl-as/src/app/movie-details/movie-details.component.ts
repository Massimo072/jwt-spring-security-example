import {Component, Input, OnInit} from '@angular/core';
import {Movie} from '../movie';
import {MovieService} from '../service/movie.service';
import {ListMoviesComponent} from '../list-movies/list-movies.component';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {

  @Input() movie: Movie;

  constructor(private movieService: MovieService, private  listComponent: ListMoviesComponent) { }

  ngOnInit() {
  }



}
