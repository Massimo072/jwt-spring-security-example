package com.supergroup.backend.config;

import com.supergroup.backend.model.CorsEntry;
import com.supergroup.backend.security.jwt.JwtTokenFilterConfigurer;
import com.supergroup.backend.security.service.TokenService;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.OPTIONS;

@Configuration
@EnableWebSecurity

@Getter(AccessLevel.PRIVATE)
@Setter(AccessLevel.PRIVATE)
public class BackendConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

  private UserDetailsService userDetailsService;

  private TokenService tokenService;

  private PasswordEncoder passwordEncoder;

  private CorsProperties corsProperties;

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.inMemoryAuthentication().passwordEncoder(getPasswordEncoder())
      .withUser(new User("Testing123", "$2a$10$.oxAVZ7cSxJxA0jwHOUbAO/ZgIJuqa7LvpWRcy8ylBBka6pUowbMW", Collections.emptyList())); //bcrypt password : mass
  }

  @Override
  public void configure(final HttpSecurity http) throws Exception {
    String[] publicEndPoints = {
      "/",
      "/**/*.png",
      "/**/*.gif",
      "/**/*.svg",
      "/**/*.jpg",
      "/**/*.html",
      "/**/*.css",
      "/**/*.js",
      "/actuator",
      "/actuator/**/*",
      "/error",
      "/favicon.ico"
    };

    http.csrf().disable().formLogin().disable().httpBasic().and().cors();

    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    http.authorizeRequests().antMatchers(GET, publicEndPoints).permitAll();

    http.authorizeRequests().antMatchers(OPTIONS).permitAll();

    http.authorizeRequests().anyRequest().fullyAuthenticated();

    http.apply(new JwtTokenFilterConfigurer(authenticationManager(), getTokenService()));
  }

  @Bean
  CorsConfigurationSource corsConfigurationSource() {
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    if (getCorsProperties().getEnabled()) {
      List<CorsEntry> corsEntries = getCorsProperties().getEntries();
      if (corsEntries != null && !corsEntries.isEmpty()) {
        for (final CorsEntry entry : corsEntries) {
          entry.getVerbs().add(OPTIONS.name());
          CustomCorsConfiguration corsConfig = new CustomCorsConfiguration();
          corsConfig.applyPermitDefaultValues();
          corsConfig.setAllowedOrigins(entry.getOrigins());
          corsConfig.setAllowedMethods(entry.getVerbs());
          source.registerCorsConfiguration(entry.getEndpoint(), corsConfig);
        }
      }
    }
    return source;
  }

  @Resource(name = "jwtTokenService")
  public void setJwtTokenProvider(final TokenService tokenService) {
    this.tokenService = tokenService;
  }

  @Resource(name = "passwordEncoder")
  public void setPasswordEncoder(final PasswordEncoder passwordEncoder) {
    this.passwordEncoder = passwordEncoder;
  }

  @Resource(name = "com.supergroup.backend.cors-com.supergroup.backend.config.CorsProperties")
  public void setCorsProperties(final CorsProperties corsProperties) {
    this.corsProperties = corsProperties;
  }

}
