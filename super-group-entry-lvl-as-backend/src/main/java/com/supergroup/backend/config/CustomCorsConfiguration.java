package com.supergroup.backend.config;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.cors.CorsConfiguration;

@Component
public class CustomCorsConfiguration extends CorsConfiguration {
  /**
   * Wildcard representing <em>all</em> origins, methods, or headers.
   */
  private static final String ALL = "*";

  /**
   * Check the origin and determine the origin for the response. The default
   * implementation simply delegates to
   * {@link CorsConfiguration#checkOrigin(String)}.
   */
  @Nullable
  @Override
  public String checkOrigin(@Nullable String requestOrigin) {
    if (!StringUtils.hasText(requestOrigin)) {
      return null;
    }
    if (ObjectUtils.isEmpty(this.getAllowedOrigins())) {
      return null;
    }
    if (this.getAllowedOrigins().contains(ALL)) {
      if (this.getAllowCredentials() != null && !this.getAllowCredentials()) {
        return ALL;
      } else {
        return requestOrigin;
      }
    }
    String reqHost = requestOrigin.replace("https://", "").replace("http://", "");
    if (reqHost.endsWith("/")) {
      reqHost = reqHost.substring(0, reqHost.length() - 1);
    }
    for (String allowedOrigin : this.getAllowedOrigins()) {
      if (reqHost.equalsIgnoreCase(allowedOrigin)) {
        return requestOrigin;
      }
    }
    return null;
  }
}
