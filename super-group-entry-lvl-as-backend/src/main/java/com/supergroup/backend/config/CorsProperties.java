package com.supergroup.backend.config;

import com.supergroup.backend.model.CorsEntry;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Getter
@Setter
@ConfigurationProperties("com.supergroup.backend.cors")
public class CorsProperties {

  private Boolean enabled;

  private List<CorsEntry> entries;

  public Boolean isEnabled() {
    return enabled != null && enabled;
  }

  public Boolean getEnabled() {
    return isEnabled();
  }

}
