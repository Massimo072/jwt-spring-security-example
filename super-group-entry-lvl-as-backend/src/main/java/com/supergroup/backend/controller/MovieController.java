package com.supergroup.backend.controller;


import com.supergroup.backend.model.Movie;
import com.supergroup.backend.repo.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class MovieController {

  @Autowired
  MovieRepository repository;

  @GetMapping("/movies")
  public List<Movie> getAllMovies() {
    List<Movie> movies = new ArrayList<>();
    repository.findAll().forEach(movies::add);
    return movies;
  }

  @GetMapping("/movies/getMovie/{id}")
  public Optional<Movie> getMovie(@PathVariable Long id) {
    return repository.findById(id);
  }

  @PostMapping(value = "/movies/add")
  public Movie postMovie(@RequestBody Movie movie) {
    Movie _movie = repository.save(new Movie(movie.getName(), movie.getGenre(), movie.getReleaseDate(), movie.getDirector(), movie.getDescription()));
    return _movie;
  }
    @DeleteMapping("/movies/delete/{id}")
    public ResponseEntity<String> deleteMovie(@PathVariable("id") long id) {
        repository.deleteById(id);
        return new ResponseEntity<>("Movie has been deleted!", HttpStatus.OK);
    }

    @PutMapping("/movies/update/{id}")
    public ResponseEntity<Movie> updateMovie    (@PathVariable("id") long id, @RequestBody Movie movie) {
        Optional<Movie> movieData = repository.findById(id);
        if (movieData.isPresent()) {
            Movie _movie = movieData.get();
            _movie.setName(movie.getName());
            _movie.setGenre(movie.getGenre());
            _movie.setReleaseDate(movie.getReleaseDate());
            _movie.setDirector(movie.getDirector());
            _movie.setDescription(movie.getDescription());
            return new ResponseEntity<>(repository.save(_movie), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

   /* @GetMapping(produces = "application/json")
    @RequestMapping({ "/validateLogin" })
    public User validateLogin() {
        return new User("User successfully authenticated");
    }*/
}
