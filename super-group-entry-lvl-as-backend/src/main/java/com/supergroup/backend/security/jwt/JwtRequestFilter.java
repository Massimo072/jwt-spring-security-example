package com.supergroup.backend.security.jwt;

import com.supergroup.backend.security.service.TokenService;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Getter(AccessLevel.PRIVATE)
@Setter(AccessLevel.PRIVATE)
class JwtRequestFilter extends OncePerRequestFilter {

  private AuthenticationManager authenticationManager;

  private TokenService tokenService;

  JwtRequestFilter(final AuthenticationManager authenticationManager,
                   final TokenService tokenService) {
    setAuthenticationManager(authenticationManager);
    setTokenService(tokenService);
  }

  @Override
  protected void doFilterInternal(final HttpServletRequest req,
                                  final HttpServletResponse res,
                                  final FilterChain chain) throws IOException, ServletException {
    if (SecurityContextHolder.getContext().getAuthentication() == null) {
      String token = getTokenService().resolveToken(req);
      if (token != null && !token.isEmpty()) {
        UserDetails userDetails = getTokenService().getUserDetails(token);
        if (userDetails != null) {
          UsernamePasswordAuthenticationToken userToken = new UsernamePasswordAuthenticationToken(
            userDetails, null, userDetails.getAuthorities()
          );
          userToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
          SecurityContextHolder.getContext().setAuthentication(userToken);
        }
      }
    }
    chain.doFilter(req, res);
  }
}
