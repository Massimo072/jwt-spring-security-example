package com.supergroup.backend.security.domain;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import static java.util.Collections.emptyList;

@Getter
@Setter
public class LoginRequest {

  @JsonAlias({"email", "username"})
  private String username;

  private String password;

  public UsernamePasswordAuthenticationToken getAuthToken() {
    return new UsernamePasswordAuthenticationToken(getUsername(), getPassword(), emptyList());
  }
}
