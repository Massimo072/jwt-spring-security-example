package com.supergroup.backend.security.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.http.HttpServletRequest;

public interface TokenService {

  UserDetails getUserDetails(final String token);

  String generateToken(final Authentication authentication);

  String resolveToken(final HttpServletRequest req);

  String getTokenHeader();

  String getTokenPrefix();

  Boolean verifyToken(final String token);



}
