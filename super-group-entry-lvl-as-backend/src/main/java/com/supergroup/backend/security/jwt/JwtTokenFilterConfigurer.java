package com.supergroup.backend.security.jwt;

import com.supergroup.backend.security.service.TokenService;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Getter(AccessLevel.PRIVATE)
@Setter(AccessLevel.PRIVATE)
public class JwtTokenFilterConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

  private AuthenticationManager authenticationManager;

  private TokenService tokenService;

  public JwtTokenFilterConfigurer(final AuthenticationManager authenticationManager,
                                  final TokenService tokenService) {
    setAuthenticationManager(authenticationManager);
    setTokenService(tokenService);
  }

  @Override
  public void configure(final HttpSecurity http) {
    http.addFilterBefore(new JwtLoginFilter(getAuthenticationManager(), getTokenService()), UsernamePasswordAuthenticationFilter.class);
    http.addFilterBefore(new JwtRequestFilter(getAuthenticationManager(), getTokenService()), JwtLoginFilter.class);
  }
}
