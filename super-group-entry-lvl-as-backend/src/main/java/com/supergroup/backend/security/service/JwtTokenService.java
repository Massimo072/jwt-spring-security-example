package com.supergroup.backend.security.service;

import com.supergroup.backend.security.util.RSAKeyProvider;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalTime;
import java.util.Date;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Setter(AccessLevel.PRIVATE)
@Service("jwtTokenService")
public class JwtTokenService implements TokenService {

  private static final Logger logger = LoggerFactory.getLogger(JwtTokenService.class);

  private static LocalTime startedTime;

  private RSAKeyProvider rsaKeyProvider;

  private UserDetailsService userDetailsService;

  private Integer expirationTime;

  private String tokenPrefix;

  private String tokenHeader;

  @PostConstruct
  public void postConstruct() {
    setRsaKeyProvider(new RSAKeyProvider());
    JwtTokenService.startedTime = LocalTime.now();
  }

  @Override
  public UserDetails getUserDetails(final String token) {
    if (token != null && !token.isEmpty() && verifyToken(token)) {
      UserDetails userDetails = getUserDetailsService().loadUserByUsername(getUserIdFromToken(token));
      if (userDetails == null) {
        userDetails = getUserDetailsService().loadUserByUsername(getUsernameFromToken(token));
      }
      return userDetails;
    }
    return null;
  }

  @Override
  public String generateToken(final Authentication authentication) {
    UserDetails userPrincipal = (UserDetails) authentication.getPrincipal();
    Date now = new Date();
    Claims claims = Jwts.claims()
      .setExpiration(new Date(now.getTime() + getExpirationTime()))
      .setId(userPrincipal.getUsername())
      .setIssuedAt(now)
      .setSubject(userPrincipal.getUsername());
    claims.put("auth", userPrincipal.getAuthorities().stream().filter(Objects::nonNull)
      .map(GrantedAuthority::getAuthority)
      .map(SimpleGrantedAuthority::new)
      .collect(Collectors.toList()));
    return Jwts.builder()
      .setClaims(claims)
      .signWith(SignatureAlgorithm.RS512, getRsaKeyProvider().getPrivateKey())
      .compact();
  }

  @Override
  public String resolveToken(final HttpServletRequest req) {
    String bearerToken = req.getHeader(getTokenHeader());
    if (bearerToken != null && !bearerToken.isEmpty() && bearerToken.startsWith(getTokenPrefix())) {
      return bearerToken.replace(getTokenPrefix(), "");
    }
    return null;
  }

  @Override
  public Boolean verifyToken(final String token) {
    try {
      Jwts.parser().setSigningKey(getRsaKeyProvider().getPublicKey()).parseClaimsJws(token);
    } catch (final ExpiredJwtException | IllegalArgumentException | MalformedJwtException | SignatureException | UnsupportedJwtException ex) {
      logger.info("Failed to validate the JWT.", ex);
      return false;
    }
    return true;
  }

  @Resource
  public void setUserDetailsService(final UserDetailsService userDetailsService) {
    this.userDetailsService = userDetailsService;
  }

  @Value("3600000")
  public void setExpirationTime(final Integer expirationTime) {
    this.expirationTime = expirationTime;
  }

  @Value("Bearer ")
  public void setTokenPrefix(final String tokenPrefix) {
    this.tokenPrefix = tokenPrefix;
  }

  @Value("Authorization")
  public void setTokenHeader(final String tokenHeader) {
    this.tokenHeader = tokenHeader;
  }

  private String getUserIdFromToken(final String token) {
    return parseTokenClaims(token).getBody().getId();
  }

  private String getUsernameFromToken(final String token) {
    return parseTokenClaims(token).getBody().getSubject();
  }

  private Jws<Claims> parseTokenClaims(final String token) {
    return Jwts.parser().setSigningKey(getRsaKeyProvider().getPublicKey()).parseClaimsJws(token);
  }
}
