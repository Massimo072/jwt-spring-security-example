package com.supergroup.backend.security.util;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

import io.jsonwebtoken.impl.crypto.RsaProvider;

@Getter(AccessLevel.PRIVATE)
@Setter(AccessLevel.PRIVATE)
public class RSAKeyProvider {

  private KeyPair keyPair;

  public RSAKeyProvider() {
    setKeyPair(RsaProvider.generateKeyPair());
  }

  public PublicKey getPublicKey() {
    return getKeyPair().getPublic();
  }

  public PrivateKey getPrivateKey() {
    return getKeyPair().getPrivate();
  }
}
