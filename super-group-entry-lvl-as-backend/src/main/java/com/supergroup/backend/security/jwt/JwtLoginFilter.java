package com.supergroup.backend.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.supergroup.backend.security.domain.LoginRequest;
import com.supergroup.backend.security.service.TokenService;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.http.HttpMethod.POST;

@Getter(AccessLevel.PRIVATE)
@Setter(AccessLevel.PRIVATE)
class JwtLoginFilter extends AbstractAuthenticationProcessingFilter {

  private TokenService tokenService;

  JwtLoginFilter(final AuthenticationManager authManager, final TokenService tokenService) {
    super(new AntPathRequestMatcher("/login/jwt"));
    setAuthenticationManager(authManager);
    setTokenService(tokenService);
  }

  @Override
  public Authentication attemptAuthentication(final HttpServletRequest req,
                                              final HttpServletResponse res) throws AuthenticationException {
    try {
      if (req.getMethod().equals(POST.name())) {
        LoginRequest request = new ObjectMapper().readValue(req.getInputStream(), LoginRequest.class);
        if (request != null) {
          Authentication authentication = getAuthenticationManager().authenticate(request.getAuthToken());
          SecurityContextHolder.getContext().setAuthentication(authentication);
          return authentication;
        }
      }
    } catch (IOException ioe) {
      // Likely did not post the object with, or it is not a post request
      throw new BadCredentialsException("Could not successfully read/process user credentials.");
    }
    return null;
  }

  @Override
  protected void successfulAuthentication(final HttpServletRequest req,
                                          final HttpServletResponse res,
                                          final FilterChain chain,
                                          final Authentication auth) throws IOException {
    res.setContentType("application/json");
    res.setCharacterEncoding("UTF-8");
    res.getWriter().write(
      String.format("{\"jwt\":\"%s\"}", getTokenService().getTokenPrefix().concat(getTokenService().generateToken(auth)))
    );
  }
}
