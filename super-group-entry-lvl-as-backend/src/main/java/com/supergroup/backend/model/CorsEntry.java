package com.supergroup.backend.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CorsEntry {

  private String endpoint;

  private List<String> origins;

  private List<String> verbs;

}
